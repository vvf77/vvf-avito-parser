#!/usr/bin/node  --max-old-space-size=6000

'use strinct';
'jslint node: true';
/*jslint node: true */

var request_original = require('request');

var userAgentString = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/36.0.1985.125 Chrome/36.0.1985.125 Safari/537.36';
var mobileUserAgentString = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53';
request = request_original.defaults({ headers:{'User-Agent':userAgentString}});
var cookieJar = request.jar();
var cheerio = require('cheerio');
var async= require('async');
var fs = require('fs');

var EventEmitter = require("events").EventEmitter;
var util = require('util');
var path = require('path');

var log_debug = false;
var numberPagesToTest = null;

var default_config = {
    /* **** config ***** */
    numberParalleleDownloads: 5,
    numberParalleleImageParser: null,
    startUrls: [
        'http://www.avito.ru/rossiya/nedvizhimost',
    ],
    proxies:[
         '210.101.131.231:8080',
         '31.28.6.13:8118',
         '41.72.105.38:3128',
         '42.117.3.73:3128',
         '41.223.119.156:3128'
    ],
    maxTries:10,
    
    minPage:1,
    maxPage:5,

    timeoutPerTry:2000,

    onParseOne: null,
    onImageReaded:null
    /* **** /config ***** */
};

var decode64=function( base64string ){ return new Buffer( base64string, 'base64' ).toString('utf8'); };

// Функция принимает задачу на загрузку и выполняет ее: просто запускает нужную функцию в зависмости от типа задачи. 
var worker = function( task, next ){
    task.tryNumber = 1+task.tryNumber||0;
    var self = this;
    // if error occured queue this task again, but not more than 15 times.
    var restartTask = function(){
        if( task.tryNumber > 0){
            self.proxyNumber++;
            var proxy = null;
            if( self.config.proxies ){
                if( self.proxyNumber >= self.config.proxies.length ){
                    self.proxyNumber = -1;
                }else{
                    proxy = self.config.proxies[ self.proxyNumber ];
                }
            }
            if( log_debug ) console.log( 'Through proxy=', proxy);
            request = request_original.defaults({ headers:{'User-Agent':userAgentString}, proxy:'http://'+proxy});
        }
        if( task.tryNumber > self.config.maxTries ){
            self.emit('error', 'errorLoad', task);
            if( log_debug ) console.log('Error load after '+self.config.maxTries+' tries', task );
            return;
        }
        self.loaderQueue.push( task );
    };
    if( 'list' == task.type ){
        task.page = task.page || self.config.minPage ||  1;
        self.AvitoParseList( task, function( err ){
            if( 'nopage' == err ){
                self.emit('lastPage', task );
                if(log_debug) console.log( 'Page %s was last. Do not continue lists load', task.page - 1);
            }else if( err ){
                if( log_debug ) console.log(' restart task by error', err);
                self.emit('restart', task.tryNumber, task );
                setTimeout( restartTask, self.config.timeoutPerTry*task.tryNumber+100);
            }else{
                task.tryNumber = 0;
                task.page++;
                if( self.config.maxPage && task.page > self.config.maxPage ){
                    self.emit('lastPage', task );
                }else{
                    self.loaderQueue.push( task );
                }
            }
            setTimeout(next , 1 );
        } );
    }else{
        this.AvitoParseOne( task.url, function( err ){
            if( err ){
                self.emit('error',err, task);
                setTimeout(restartTask, 300*task.tryNumber+100);
            }
            setTimeout(next , 1 );
        } );
    }
};

var AvitoParser = function( params ){
    // resultCallback, recogonizedImagesCallback
    var self = this;
    this.proxyNumber = -1;
    this.config = {};
    for(var i in default_config ){
        if( default_config.hasOwnProperty(i)){
            this.config[i] = default_config[i];
        }
    }
    util._extend( this.config, params );

    this.loaderQueue = async.queue(worker.bind( this ), this.config.numberParalleleDownloads);
    if( 'function' == typeof this.config.onParseOne ){
        self.on('parseOne', this.config.onParseOne);
    }
    if( this.config.onImageReaded || this.config.imageRecogonize){
        this.recogonizeImagesQueue = async.queue( imgParser.bind( this ), this.config.numberParalleleImageParser || this.config.numberParalleleDownloads);
        if( typeof this.config.onImageRead == 'function'){
            this.on('imageReaded', this.config.onImageReaded );
        }
    }
    this.count = 0;
    if( 'function' == typeof this.config.onDone){
        this.on('done', this.config.onDone);
    }
    this.loaderQueue.drain = function(){
        self.emit('done', self.count );
    };

    this.run = function(){
        self.config.startUrls.forEach( function(url){
            self.loaderQueue.push({ url:url, type:'list'});
        });
        for(var i in arguments) if(arguments.hasOwnProperty(i)){
            self.loaderQueue.push({ url:arguments[i], type:'list'});
        }
    };
};

util.inherits(AvitoParser, EventEmitter);

AvitoParser.prototype.AvitoParseList = function( task, next ){
    var self = this;
    if( log_debug ) console.log('List: ', task.url );
    request({ url: task.url + '?p='+task.page ,  jar:cookieJar, method:'GET'}, function( err, resp, body ){
        if( err ) return next( err );
        if( !body ) return next('Empty page ' + task.url);
        var $ = cheerio.load( body );
        var page = $('.pagination__page_current').html();
        //if( log_debug ) console.log('current page: ', page );
        if( self.numberPagesToTest && page > self.numberPagesToTest) return next('nopage');
        // if don't need "hidden pages": 
        // if( task.page>1 && !page ) return  next('nopage');
        // if parse "hidden pages"
        if( task.page>1 && '1' == page ) return next('nopage');
        $( '.item .title a' ).each( function(i,a){
            self.loaderQueue.push({ url:'http://www.avito.ru'+$(a).attr('href'), type:'one' });
        });
        $= null;
        body=null;
        next();
    });
};

AvitoParser.prototype.AvitoParseOne = function( url, next ){
    var self = this;
    var advUrl = url;
    //if( log_debug ) console.log('One : ', url );
    request({ url: url, jar:cookieJar, method:'GET'}, function( err, resp, body ){
        if(err){
            return next( err );
        }
        if( !body ) return next('Empty page '+ advUrl);

        self.count ++ ;
        var phoneJar = request.jar();
        var mUrl = advUrl.replace('http://www.avito.ru','https://m.avito.ru');
        //console.log( 'mobile: ', mUrl);
        request({ url: mUrl,
            jar:phoneJar, method:'GET',
            headers:{'User-Agent':mobileUserAgentString }
            }, function( err, resp, mbody ){
            if(err){
                return next( err );
            }
            if( !mbody ) return next('Empty page mobile version');
            var $ = cheerio.load( mbody );
            var jUrl = $('.action-show-number').attr('href');
            jUrl = 'https://m.avito.ru'+jUrl+'?async';
            request({ url: jUrl,
                headers:{'User-Agent':mobileUserAgentString,
                    Referer:mUrl},
                jar:phoneJar, method:'GET'}, function( err, resp, sJson ){
                if(err){
                    return next( err );
                }
                var data = {phone:null};
                if( !sJson ) return next('Empty json');
                try{
                    data = JSON.parse( sJson );
                }catch(e){
                    next('Json phone parse error');
                }
                phoneJar = null;
                self.emit('parseOne', null, url, body, data.phone );
                next();
            });
            $= null;
            mbody=null;
            //next();
        });

        //next(); // continue load other pages in paralell with image loading
    });
};

AvitoParser.prototype.setDebugMode = function(){ log_debug = true; };

var tests={
    count:0,
    resultCallback: function( error, url, html, phone ){
        if( !html ){
            console.log('FAIL: html empty');
        }
        if( !phone ){
            console.log('FAIL: phone empty');
        }
        $=null;
        tests.count ++;
        console.log(tests.count, this.count, phone, html.length);
        //console.log( 'Saved phone images: ', images);
    },
    run: function(){
        var parser = new AvitoParser({ onParseOne: tests.resultCallback });
        parser.numberPagesToTest = 1;
        parser.on('error', function(){ console.log('error:', arguments); })
            .on('restart', console.log )
            .run();
        //resultCallback = tests.resultCallback;
    }
};
var main=function(){
    tests.run();
};

if( !module.parent ){
    log_debug = true;
    //numberPagesToTest = 2;
    main();
}
module.exports = AvitoParser;
